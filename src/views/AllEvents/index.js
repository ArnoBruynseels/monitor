import React, { useEffect } from 'react';
import CustomMaterialTable from '../../components/CustomMaterialTable';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';
import Loader from '../../components/Loader';

const AllEvents = () => {
  const store = useStore();

  useEffect(() => {
    store.events.allEvents.fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Loader type="circular" loading={store.events.allEvents.loading} size={75}>
      <CustomMaterialTable data={store.events.allEvents.value} />
    </Loader>
  );
};

export default observer(AllEvents);
