import React, { useEffect } from 'react';
import { useStore } from '../../mobx';
import Loader from '../../components/Loader';
import CustomMaterialTable from '../../components/CustomMaterialTable';
import { observer } from 'mobx-react-lite';

const QueuedEvents = () => {
  const store = useStore();

  useEffect(() => {
    store.events.queuedEvents.fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Loader
      type="circular"
      loading={store.events.queuedEvents.loading}
      size={75}
    >
      <CustomMaterialTable data={store.events.queuedEvents.value} />
    </Loader>
  );
};

export default observer(QueuedEvents);
