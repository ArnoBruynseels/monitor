import React from 'react';
import * as PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Icon } from 'react-icons-kit';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  link: {
    textDecoration: 'none',
    color: 'black',
  },
}));

const MainNavigationList = props => {
  const classes = useStyles();

  const { navItems } = props;

  return (
    <List>
      {navItems.map(navItem => (
        <Link to={navItem.path} className={classes.link} key={navItem.path}>
          <ListItem button key={navItem.text}>
            <ListItemIcon style={{ marginLeft: '5px' }}>
              <Icon icon={navItem.icon} size={navItem.iconSize} />
            </ListItemIcon>
            <ListItemText primary={navItem.text} />
          </ListItem>
        </Link>
      ))}
    </List>
  );
};

MainNavigationList.propTypes = {
  navItems: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      icon: PropTypes.object.isRequired,
      iconSize: PropTypes.number,
    })
  ).isRequired,
};

MainNavigationList.defaultProps = {};

export default MainNavigationList;
