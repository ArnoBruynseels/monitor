import React from 'react';
import * as PropTypes from 'prop-types';
import MaterialTable from 'material-table';

const CustomMaterialTable = props => {
  const { data } = props;

  return (
    <MaterialTable
      title="All Events"
      columns={[
        {
          title: 'Topic',
          field: 'topic',
        },
        {
          title: 'Event',
          field: 'event',
        },
        {
          title: 'Begin Time',
          field: 'beginTime',
        },
        {
          title: 'End Time',
          field: 'endTime',
        },
        {
          title: 'Service',
          field: 'message.service.id',
        },
        {
          title: 'Release',
          field: 'message.release.id',
        },
        {
          title: 'Subzone',
          field: 'message.subzone.id',
        },
        {
          title: 'Policy',
          field: 'message.policy.code',
        },
      ]}
      data={data}
      options={{
        filtering: true,
      }}
    />
  );
};

CustomMaterialTable.propTypes = {
  data: PropTypes.array.isRequired,
};

CustomMaterialTable.defaultProps = {};

export default CustomMaterialTable;
