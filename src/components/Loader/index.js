import React from 'react';
import * as PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';

const Loader = props => {
  const { loading, type, size, color, style, children } = props;

  if (loading) {
    return (
      <div style={style}>
        {type === 'circular' ? (
          <CircularProgress color={color} size={size} />
        ) : (
          <LinearProgress color={color} />
        )}
      </div>
    );
  }

  return <>{children}</>;
};

Loader.propTypes = {
  loading: PropTypes.bool,
  type: PropTypes.oneOf(['linear', 'circular']),
  size: PropTypes.number,
  style: PropTypes.object,
  color: PropTypes.string,
};

Loader.defaultProps = {
  type: 'circular',
  loading: false,
  size: 40,
  style: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 'calc(100vh - 150px)',
  },
  color: 'primary',
};

export default Loader;
