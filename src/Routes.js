import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import navigationUrls from './constants/navigation-urls';
import QueuedEvents from './views/QueuedEvents/index';
import AllEvents from './views/AllEvents/index';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to={navigationUrls.ALL_EVENTS} />
      </Route>
      <Route exact path={navigationUrls.ALL_EVENTS}>
        <AllEvents />
      </Route>
      <Route exact path={navigationUrls.QUEUED_EVENTS}>
        <QueuedEvents />
      </Route>
    </Switch>
  );
};

export default Routes;
