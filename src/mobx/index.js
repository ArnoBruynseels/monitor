import React from 'react';
import { useLocalStore } from 'mobx-react-lite';
import axiosUtil from '../utils/axios.util';
import events from './events';

const dependencies = {
  axios: axiosUtil,
};

const store = () => ({
  events: events(dependencies),
});

const storeContext = React.createContext({});

export const StoreProvider = ({ children }) => {
  return (
    <storeContext.Provider value={useLocalStore(store)}>
      {children}
    </storeContext.Provider>
  );
};

export const useStore = () => {
  const store = React.useContext(storeContext);
  if (!store) {
    throw new Error('You have forgot to use StoreProvider, shame on you.');
  }
  return store;
};
