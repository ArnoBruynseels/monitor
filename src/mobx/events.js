export default function branches({ axios }) {
  return {
    allEvents: {
      loading: false,
      value: [],
      async fetch() {
        this.loading = true;

        const response = await axios.request({
          url: `http://localhost:3000/api/event/all`,
        });

        this.value = response.data;

        this.loading = false;
      },
    },
    queuedEvents: {
      loading: false,
      value: [],
      async fetch() {
        this.loading = true;

        const response = await axios.request({
          url: `http://localhost:3000/api/event/queued`,
        });

        this.value = response.data;

        this.loading = false;
      },
    },
    get loading() {
      return this.allEvents.loading || this.queuedEvents.loading;
    },
  };
}
