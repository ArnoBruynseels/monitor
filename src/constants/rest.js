export const REQUEST_PREFIX = '[REQ]';
export const RESPONSE_PREFIX = '[RES]';

export const GET = 'get';
export const POST = 'post';
export const PUT = 'put';
export const DELETE = 'delete';
