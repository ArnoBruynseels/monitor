const EVENTS_RUL_PREFIX = '/events';

const navigationUrls = {
  ALL_EVENTS: `${EVENTS_RUL_PREFIX}/all`,
  QUEUED_EVENTS: `${EVENTS_RUL_PREFIX}/queued`,
};

export default navigationUrls;
