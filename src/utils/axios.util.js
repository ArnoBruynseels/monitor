import axios from 'axios';
import { logApiRequestToConsole } from './log.util';
import { REQUEST_PREFIX, RESPONSE_PREFIX } from '../constants/rest';

const getAxiosInstance = () => {
  const axiosInstance = axios.create({});

  const ENABLE_API_LOGGING = process.env.REACT_APP_ENABLE_REST_LOGGING;

  if (ENABLE_API_LOGGING.toLocaleLowerCase() === 'true') {
    axiosInstance.interceptors.request.use(request => {
      const method = request.method.toUpperCase();
      const url = request.url;

      let message = `${method} ${url}`;

      if (request.params && typeof request.params === 'object') {
        const queryParams = request.params;

        Object.keys(queryParams).forEach((paramKey, index) => {
          if (index === 0) {
            message = `${message}?${paramKey}=${queryParams[paramKey]}`;
          } else {
            message = `${message}&${paramKey}=${queryParams[paramKey]}`;
          }
        });
      }

      logApiRequestToConsole(REQUEST_PREFIX, message, request.data);

      return request;
    });

    axiosInstance.interceptors.response.use(response => {
      const method = response.config.method.toUpperCase();
      const url = response.request.responseURL;

      const message = `${method} ${url}`;

      logApiRequestToConsole(
        RESPONSE_PREFIX,
        message,
        response.data,
        response.status
      );

      return response;
    });
  }

  return axiosInstance;
};

export default getAxiosInstance();
