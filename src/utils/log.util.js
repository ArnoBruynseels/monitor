import { REQUEST_PREFIX } from '../constants/rest';

export const logApiRequestToConsole = (
  prefix,
  message,
  content = null,
  status = 200
) => {
  let statusColor;

  if (status === 200) {
    statusColor = 'color: green;';
  } else {
    statusColor = 'color: red;';
  }

  let prefixColor;

  if (prefix === REQUEST_PREFIX) {
    prefixColor = 'color: blue;';
  } else {
    prefixColor = 'color: orange;';
  }

  if (content) {
    console.groupCollapsed(
      `%c ${prefix} %c ${message}`,
      prefixColor,
      statusColor
    );
    console.log(content);
    console.groupEnd();
  } else {
    console.log(`%c ${prefix} %c ${message}`, prefixColor, statusColor);
  }
};
