# An Event monitor

This is a monitoring dashboard for events that are consumed by a Kafka broker.

Below are some screenshots of what is looks like.

![Orchestrator Logic Flow](./monitor-pictures/monitor-1.PNG)

![Orchestrator Logic Flow](./monitor-pictures/monitor-2.PNG)

![Orchestrator Logic Flow](./monitor-pictures/monitor-3.PNG)

![Orchestrator Logic Flow](./monitor-pictures/monitor-4.PNG)

![Orchestrator Logic Flow](./monitor-pictures/web-console.PNG)
